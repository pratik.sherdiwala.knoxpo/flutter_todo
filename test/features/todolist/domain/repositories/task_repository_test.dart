import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/data/repositories/task_repository_impl.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:mockito/mockito.dart';

class MockFirebaseDataSource extends Mock implements FirebaseDataSource {}

void main() {
  TaskRepositoryImpl repositoryImpl;
  MockFirebaseDataSource mockFirebaseDataSource;

  setUp(() {
    mockFirebaseDataSource = MockFirebaseDataSource();
    repositoryImpl = TaskRepositoryImpl(dataSource: mockFirebaseDataSource);
  });

  group('getTasklist', () {
    final taskList = [
      TodoModel(
          name: 'Todo 1',
          description: 'go to gym',
          isCompleted: true,
          docId: '')
    ];

    test('should return a tasklist', () async {
      when(mockFirebaseDataSource.getTodoList())
          .thenAnswer((_) => Future.value(taskList));

      final result = await repositoryImpl.getTaskList();

      verify(mockFirebaseDataSource.getTodoList());

      expect(result, equals(Right(taskList)));
    });
  });

  test('should fail while fetching list', () async {
    when(mockFirebaseDataSource.getTodoList()).thenThrow(ConnectionFailure());

    final result = await repositoryImpl.getTaskList();

    expect(result, equals(Left(ConnectionFailure())));
  });

  group('delete todo', () {
    test('should delete todo successfully ', () async {
      when(mockFirebaseDataSource.deleteTask(any))
          .thenAnswer((_) => Future.value(true));

      final result = await repositoryImpl.deleteTask('docId');

      expect(result, equals(Right(true)));
    });

    test('should thorw error while deleting a todo', () async {
      when(mockFirebaseDataSource.deleteTask(any)).thenThrow(TaskFailure());

      final result = await repositoryImpl.deleteTask('docId');

      expect(result, equals(Left(TaskFailure())));
    });
  });

  group('reorder list', () {
    final tTodo = Todo(
      name: 'Todo 1',
      description: 'todo one',
      isCompleted: true,
      sortNumber: 2,
    );

    test('should reorder list', () async {
      when(mockFirebaseDataSource.reorderList(tTodo))
          .thenAnswer((_) async => Future.value(''));

      final result = await repositoryImpl.reorderTodoList(tTodo);

      verify(repositoryImpl.reorderTodoList(tTodo));

      expect(result, equals(Right('somestring')));
    });

    test('should fail to reorder list', () async {
      when(mockFirebaseDataSource.reorderList(tTodo)).thenThrow(TaskFailure());

      final result = await repositoryImpl.reorderTodoList(tTodo);

      verify(repositoryImpl.reorderTodoList(tTodo));

      expect(result, equals(Left(TaskFailure())));
    });
  });
}
