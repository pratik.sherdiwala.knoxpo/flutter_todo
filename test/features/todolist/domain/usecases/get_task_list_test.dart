import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/fetch_tasks.dart';
import 'package:mockito/mockito.dart';

class MockTaskRepository extends Mock implements TaskRepository {}

void main() {
  final List<Todo> taskList = [
    Todo(name: "Todo1", description: "go to gym", isCompleted: false),
    Todo(name: "Todo2", description: "go to office", isCompleted: true),
    Todo(name: "Todo3", description: "go to temple", isCompleted: false),
  ];

  FetchTaskListUsecase fetchTaskListUsecase;
  MockTaskRepository mockTaskRepository;

  setUp(() {
    mockTaskRepository = MockTaskRepository();
    fetchTaskListUsecase =
        FetchTaskListUsecase(taskRepository: mockTaskRepository);
  });

  group('get task list', () {
    test('should fetch all tasks from firebase', () async {
      when(mockTaskRepository.getTaskList())
          .thenAnswer((_) async => Right(taskList));

      final result = await fetchTaskListUsecase(NoParams());

      expect(result, Right(taskList));
    });

    test('should fail to fetch task list ', () async {
      when(mockTaskRepository.getTaskList()).thenReturn(null);

      final result = await fetchTaskListUsecase(NoParams());

      expect(result, null);
    });
  });
}
