import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/reorder_todolist_usecase.dart';
import 'package:mockito/mockito.dart';

class MockTaskRepository extends Mock implements TaskRepository {}

void main() {
  final tTodo = Todo(
    name: 'Todo 1',
    description: 'todo one',
    isCompleted: true,
    sortNumber: 2,
  );

  TodoReorderUseCase reorderUseCase;
  MockTaskRepository mockTaskRepository;

  setUp(() {
    mockTaskRepository = MockTaskRepository();
    reorderUseCase = TodoReorderUseCase(repository: mockTaskRepository);
  });

  group('reorder todo list', () {
    test('should reorder list', () async {
      when(mockTaskRepository.reorderTodoList(tTodo))
          .thenAnswer((_) async => Right('docId'));

      final result = await reorderUseCase(tTodo);

      expect(result, equals(Right('docId')));
    });

    test('shoudl fail while reordering the lis ', () async {
      when(mockTaskRepository.reorderTodoList(tTodo)).thenThrow(TaskFailure());

      final result = await reorderUseCase(tTodo);

      expect(result, equals(Left(TaskFailure())));
    });
  });
}
