import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/delete_todo_usecase.dart';
import 'package:mockito/mockito.dart';

class MockTaskRepository extends Mock implements TaskRepository {}

void main() {
  DeleteTodoUseCase deleteTodoUseCase;
  MockTaskRepository mockTaskRepository;

  setUp(() {
    mockTaskRepository = MockTaskRepository();
    deleteTodoUseCase = DeleteTodoUseCase(taskRepository: mockTaskRepository);
  });

  group('deleteTask', () {
    test('should delete task successfully ', () async {
      when(mockTaskRepository.deleteTask(any))
          .thenAnswer((_) async => Right(true));

      final result = await deleteTodoUseCase('docId');

      expect(result, equals(Right(true)));
    });

    test('should throw error while deleting task', () async {
      when(mockTaskRepository.deleteTask(any)).thenThrow(TaskFailure());

      final result = await deleteTodoUseCase('docId');

      expect(result, equals(Left(TaskFailure())));
    });
  });
}
