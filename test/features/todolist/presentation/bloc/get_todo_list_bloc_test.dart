import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/delete_todo_usecase.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/fetch_tasks.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/reorder_todolist_usecase.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_bloc.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_event.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_state.dart';
import 'package:mockito/mockito.dart';

class MockGetTaskList extends Mock implements FetchTaskListUsecase {}

class MockDeleteTask extends Mock implements DeleteTodoUseCase {}

class MockReOrderList extends Mock implements TodoReorderUseCase {}

void main() {
  TodoBloc todoBloc;
  MockGetTaskList getTaskList;
  MockDeleteTask deleteTask;
  MockReOrderList reOrderList;

  setUp(() {
    getTaskList = MockGetTaskList();
    deleteTask = MockDeleteTask();
    reOrderList = MockReOrderList();

    todoBloc = TodoBloc(
        taskListUsecase: getTaskList,
        deleteTodoUseCase: deleteTask,
        reorderUseCase: reOrderList);
  });

  test('initial state should be Empty', () {
    expect(todoBloc.initialState, equals(Empty()));
  });

  const tError = 'Error while performing task';

  final tTaskList = [
    Todo(
      name: 'Todo 1',
      description: 'todo one',
      isCompleted: true,
      sortNumber: 5,
    ),
  ];

  final tDocId = 'docId';

  group('getTaskList', () {
    test('should emit task list successfully ', () async {
      when(getTaskList(NoParams())).thenAnswer((_) async => Right(tTaskList));

      todoBloc.dispatch(GetTodoList());

      final result = await getTaskList(NoParams());

      verify(getTaskList(NoParams()));

      expect(result, Right(tTaskList));
    });

    test('should check the states', () async {
      when(getTaskList(NoParams())).thenAnswer((_) async => Right(tTaskList));

      todoBloc.dispatch(GetTodoList());

      final expected = [
        Empty(),
        Loading(),
        Loaded(todolist: tTaskList),
      ];

      expectLater(todoBloc.state, emitsInOrder(expected));
    });

    test('should emit Error', () async {
      when(getTaskList(NoParams()))
          .thenAnswer((_) async => Left(TaskFailure()));

      todoBloc.dispatch(GetTodoList());

      final expected = [
        Empty(),
        Loading(),
        Error(message: tError),
      ];

      expectLater(todoBloc.state, emitsInOrder(expected));
    });
  });

  group('delete task', () {
    test('should delete a task successfully', () async {
      when(deleteTask(tDocId)).thenAnswer((_) async => Right(true));

      todoBloc.dispatch(DeleteTodoEvent(documentId: tDocId));

      final expected = [
        Empty(),
        Loading(),
        Loaded(todolist: tTaskList),
      ];

      expectLater(todoBloc.state, emitsInOrder(expected));
    });
  });
}
