import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/add_task_usecase.dart';
import 'package:flutter_todo/features/todo_detail/presentation/bloc/bloc.dart';
import 'package:mockito/mockito.dart';

class MockAddTodo extends Mock implements AddTaskUseCase {}

void main() {
  TodoDetailBlocBloc todoDetailBlocBloc;
  MockAddTodo mockAddTodo;

  setUp(() {
    mockAddTodo = MockAddTodo();
    todoDetailBlocBloc = TodoDetailBlocBloc(addTaskUseCase: mockAddTodo);
  });
}
