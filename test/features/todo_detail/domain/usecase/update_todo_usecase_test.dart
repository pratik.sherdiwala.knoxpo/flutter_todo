import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/update_task_usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:mockito/mockito.dart';

class MockRepository extends Mock implements TodoDetailRepository {}

void main() {
  final tTodo = Todo(
    docId: '',
    isCompleted: true,
    description: 'go to gym',
    name: 'Todo 1',
  );

  UpdateTaskUseCase updateTaskUseCase;
  MockRepository repository;

  setUp(() {
    repository = MockRepository();
    updateTaskUseCase = UpdateTaskUseCase(todoDetailRepository: repository);
  });

  group('UpdateTask', () {
    test('should update a task succesfully', () async {
      when(repository.updateTask(any))
          .thenAnswer((_) => Future.value(Right('documentId')));

      final result = await updateTaskUseCase(tTodo);

      expect(result, Right('documentId'));
    });

    test('should fail while updating status', () async {
      when(repository.updateTask(any)).thenThrow(TaskFailure());

      final result = await updateTaskUseCase(tTodo);

      expect(result, equals(Left(TaskFailure())));
    });
  });
}
