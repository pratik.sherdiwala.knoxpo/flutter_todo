import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/add_task_usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:mockito/mockito.dart';

class MockTaskRepository extends Mock implements TodoDetailRepository {}

void main() {
  final tTodo = Todo(
    docId: '',
    isCompleted: true,
    description: 'go to gym',
    name: 'Todo 1',
  );

  AddTaskUseCase addTaskUseCase;
  MockTaskRepository mockTaskRepository;

  setUp(() {
    mockTaskRepository = MockTaskRepository();
    addTaskUseCase = AddTaskUseCase(todoDetailRepository: mockTaskRepository);
  });

  group('addTask', () {
    test('should add task', () async {
      when(mockTaskRepository.insertTask(tTodo))
          .thenAnswer((_) => Future.value(Right(any)));

      final result = await addTaskUseCase(tTodo);

      expect(result, equals(Right(true)));
    });

    test('should fail to add a task', () async {
      when(mockTaskRepository.insertTask(any))
          .thenAnswer((_) => Future.value(Left(ConnectionFailure())));

      final result = await addTaskUseCase(tTodo);

      expect(result, equals(Left(ConnectionFailure())));
    });
  });
}
