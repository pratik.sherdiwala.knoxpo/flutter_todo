import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todo_detail/data/repositories/todo_detail_repository_impl.dart';
import 'package:mockito/mockito.dart';

class MockFirebaseDataSource extends Mock implements FirebaseDataSource {}

void main() {
  TodoDetailRepositoryImpl taskRepositoryImpl;
  MockFirebaseDataSource mockFirebaseDataSource;

  setUp(() {
    mockFirebaseDataSource = MockFirebaseDataSource();
    taskRepositoryImpl =
        TodoDetailRepositoryImpl(dataSource: mockFirebaseDataSource);
  });

  final tTodoModel = TodoModel(
    docId: '',
    name: 'Todo1',
    description: 'Going gym',
    isCompleted: true,
  );

  test('should insert todo in firebase', () async {
    when(mockFirebaseDataSource.insertTask(tTodoModel))
        .thenAnswer((_) => Future.value(any));

    final result = await taskRepositoryImpl.insertTask(tTodoModel);

    expect(result, equals(Right(true)));
  });

  test('should fail to add a task', () async {
    when(mockFirebaseDataSource.insertTask(any)).thenThrow(ConnectionFailure());

    final result = await taskRepositoryImpl.insertTask(tTodoModel);

    expect(result, equals(Left(ConnectionFailure())));
  });
}
