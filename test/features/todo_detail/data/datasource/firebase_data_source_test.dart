import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:mockito/mockito.dart';

class MockCollectionReference extends Mock implements CollectionReference {}

class MockFirebaseDatabase extends Mock implements Firestore {}

class MockQuerySnapshot extends Mock implements QuerySnapshot {}

class MockDocumentSnapshot extends Mock implements DocumentSnapshot {}

void main() {
  final MockCollectionReference tCollectionReference =
      MockCollectionReference();

  final DocumentSnapshot documentSnapshot = MockDocumentSnapshot();
  FirebasedataSourceImpl firebasedataSourceImpl;
  MockFirebaseDatabase mockFirebaseDatabase;
  MockQuerySnapshot mockQuerySnapshot;

  setUp(() {
    mockFirebaseDatabase = MockFirebaseDatabase();
    mockQuerySnapshot = MockQuerySnapshot();
    firebasedataSourceImpl =
        FirebasedataSourceImpl(firestore: mockFirebaseDatabase);
  });

  group('add todo data source test', () {
    final Map<String, dynamic> tData = {
      'name': "Todo2",
      'description': "go to office",
      'isCompleted': true,
    };

    test('should add todo in firestore', () async {
      when(mockFirebaseDatabase.collection('todo'))
          .thenReturn(tCollectionReference);
    });
  });
}
