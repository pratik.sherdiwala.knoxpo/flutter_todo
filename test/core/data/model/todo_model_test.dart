import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:mockito/mockito.dart';

class MockDocumentSnapShot extends Mock implements DocumentSnapshot {}

void main() {
  final tTodoModel = TodoModel(
    docId: '',
    description: 'go to gym',
    name: 'Todo1',
    isCompleted: false,
  );

  MockDocumentSnapShot documentSnapShot;

  setUp(() {
    documentSnapShot = MockDocumentSnapShot();
  });

  test('should be sub class of Todo entitiy', () {
    expect(tTodoModel, isA<Todo>());
  });

  test('should return Todo model', () {});
}
