import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:mockito/mockito.dart';

class MockCollectionReference extends Mock implements CollectionReference {}

class MockFirebaseDatabase extends Mock implements Firestore {}

class MockQuerySnapshot extends Mock implements QuerySnapshot {}

class MockDocumentSnapshot extends Mock implements DocumentSnapshot {}

class MockDocumentReference extends Mock implements DocumentReference {}

void main() {
  final MockCollectionReference tCollectionReference =
      MockCollectionReference();

  final DocumentSnapshot tDocumentSnapshot = MockDocumentSnapshot();
  final DocumentReference tDocumentReference = MockDocumentReference();

  final Map<String, dynamic> tData = {
    'name': "Todo2",
    'description': "go to office",
    'isCompleted': true,
  };

  final String tDocumentId = 'testDocumentId';

  final List<TodoModel> taskList = [
    TodoModel(
      name: "Todo2",
      description: "go to office",
      isCompleted: true,
      docId: tDocumentId,
    ),
  ];

  final tTodoModel = TodoModel(
    docId: '',
    name: 'Todo1',
    description: 'Go to gym',
    isCompleted: false,
  );

  FirebasedataSourceImpl firebasedataSourceImpl;
  MockFirebaseDatabase mockFirebaseDatabase;
  MockQuerySnapshot mockQuerySnapshot;

  setUp(() {
    mockFirebaseDatabase = MockFirebaseDatabase();

    mockQuerySnapshot = MockQuerySnapshot();

    firebasedataSourceImpl = FirebasedataSourceImpl(
        firestore: mockFirebaseDatabase, converter: null);
  });

  group('test firebase data source', () {
    test(
      'should return todo model list',
      () async {
        when(mockFirebaseDatabase.collection('todo'))
            .thenReturn(tCollectionReference);

        when(tDocumentSnapshot.data).thenReturn(tData);

        when(mockQuerySnapshot.documents).thenReturn([tDocumentSnapshot]);

        when(tCollectionReference.getDocuments())
            .thenAnswer((_) => Future.value(mockQuerySnapshot));

        final returnResult = await firebasedataSourceImpl.getTodoList();

        expect(returnResult, taskList);
      },
    );

//    test('should add todo', () async {
//      when(mockFirebaseDatabase.collection('todo'))
//          .thenReturn(tCollectionReference);
//

//      when(tDocumentSnapshot.data).thenReturn(tData);
//
//      when(tCollectionReference.add(tData))
//          .thenAnswer((_) => Future.value(tDocumentReference));
//
//      when(mockQuerySnapshot.documents).thenReturn([tDocumentSnapshot]);
//
//      final result = await firebasedataSourceImpl.insertTask(tTodoModel);
//
//      expect(result, equals(Future.value('DocumentId')));
//    });

    test('should add todo', () async {
      when(mockFirebaseDatabase.collection('todo'))
          .thenReturn(tCollectionReference);
    });

    test('should update todo', () async {
      when(mockFirebaseDatabase.collection('todo'))
          .thenReturn(tCollectionReference);

      when(tCollectionReference.document('todo'))
          .thenAnswer((_) => tDocumentReference);

      when(tDocumentReference.updateData(tData))
          .thenAnswer((_) => Future.value(Void));

      final result = await firebasedataSourceImpl.updateTask(tTodoModel);
      expect(result, equals(Right('documentId')));
    });
  });
}
