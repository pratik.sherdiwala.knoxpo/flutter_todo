import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class TodoList extends StatelessWidget {
  final Todo todo;
  final Function deleteTodo;
  final Function getTodo;

  TodoList(
      {@required this.todo, @required this.deleteTodo, @required this.getTodo});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      key: Key('${todo.name}'),
      title: Text(
        todo.name,
      ),
      subtitle: Text(todo.description),
      onTap: getTodo,
      trailing: FlatButton(
        onPressed: deleteTodo,
        child: Icon(Icons.close),
      ),
    );
  }
}
