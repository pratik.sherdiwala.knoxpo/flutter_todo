import 'package:flutter/cupertino.dart';

class MessageDisplay extends StatelessWidget {
  final String message;

  MessageDisplay({this.message});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(message),
      ),
    );
  }
}
