import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/features/todo_detail/presentation/pages/todo_detail_page.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_bloc.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_event.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_state.dart';
import 'package:flutter_todo/features/todolist/presentation/widgets/message_display.dart';
import 'package:flutter_todo/injection_container.dart';

class TodoListPage extends StatefulWidget {
  @override
  _TodoListPageState createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  TodoBloc todoBloc;
  Map<String, TodoModel> returnedMap;

  @override
  void initState() {
    super.initState();
    todoBloc = sl<TodoBloc>();
    todoBloc.dispatch(GetTodoList());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          final result = await Navigator.push(context,
              MaterialPageRoute(builder: (context) {
            return AddTodoPage(
              sortNumber: (todoBloc.state as Loaded).todolist.length.toDouble(),
            );
          }));

          Todo returnedTodo = result['todo'];

          List<Todo> todoList = (todoBloc.currentState as Loaded).todolist;

          if (todoBloc.currentState is Loaded &&
              !todoList.contains(returnedTodo)) {
            print("todo list " + returnedTodo.docId);
            returnedTodo.sortNumber = todoList.length.toDouble() + 1.0;
            todoList.add(returnedTodo);
          } else {
            todoList.forEach((todo) {
              if (returnedTodo.docId == todo.docId) {
                todo.setName(returnedTodo.name);
                todo.setDescription(returnedTodo.description);
              }
            });
          }
        },
      ),
      appBar: AppBar(
        title: Text('Todo List'),
      ),
      body: BlocBuilder<TodoBloc, TodoState>(
        bloc: todoBloc,
        builder: (_, state) {
          if (state is Loading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is Loaded) {
            return buildBody(state.todolist);
          } else if (state is Error) {
            return MessageDisplay(
              message: state.message,
            );
          } else {
            return Container(
              child: MessageDisplay(
                message: 'something went wrong',
              ),
            );
          }
        },
      ),
    );
  }

  buildBody(List<Todo> list) {
    return ReorderableListView(
      children: List.generate(list.length, (index) {
        print("Sort number: ${list[index].sortNumber}");
        return ListTile(
          key: ObjectKey('${list[index].name}'),
          onTap: () {
            final sortNumber = (list != null) ? list.length + 1 : 0;

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddTodoPage(
                          todo: list[index],
                          sortNumber: sortNumber.toDouble(),
                        )));
          },
          trailing: FlatButton(
              onPressed: () {
                todoBloc.onEvent(DeleteTodoEvent(
                  documentId: list[index].docId,
                ));
              },
              child: Icon(Icons.clear)),
          title: Text(list[index].name),
          subtitle: Text(list[index].description),
        );
      }),
      onReorder: (oldIndex, newIndex) {
        setState(() {
          if (newIndex > oldIndex) {
            newIndex -= 1;
          }
          final Todo movedTodo = list.removeAt(oldIndex);
          list.insert(newIndex, movedTodo);

          if (newIndex >= list.length - 1) {
            movedTodo.sortNumber = list[list.length - 1].sortNumber + 1.0;
          } else if (newIndex == 0) {
            movedTodo.sortNumber = list[1].sortNumber - 1.0;
          } else {
            movedTodo.sortNumber = (list[newIndex + 1].sortNumber +
                    list[newIndex - 1].sortNumber) /
                2.0;
          }

          print("New sorted number : ${movedTodo.sortNumber}");

          todoBloc.dispatch(TodoReOrderEvent(todo: movedTodo));
        });
      },
    );
  }
}

/*
* //    if (returnedMap != null) {
//      print(returnedMap['todo']);
//      list.add(returnedMap['todo']);
//      returnedMap = null;
//    }

//    return widget(
//      child: ListView.builder(
//        itemCount: list.length,
//        padding: EdgeInsets.all(10.0),
//        itemBuilder: (_, index) {
//          return TodoList(
//            todo: list[index],
//            getTodo: () {
//              Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) => AddTodoPage(
//                            todo: list[index],
//                          )));
//            },
//            deleteTodo: () {
//              todoBloc.dispatch(DeleteTodoEvent(
//                documentId: list[index].docId,
//              ));
//            },
//          );
//        },
//      ),
//    );
* */
