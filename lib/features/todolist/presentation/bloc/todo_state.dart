import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

@immutable
abstract class TodoState extends Equatable {
  @override
  List get props => [];
}

class Empty extends TodoState {}

class Loading extends TodoState {}

class Loaded extends TodoState {
  final List<Todo> todolist;

  Loaded({@required this.todolist});

  @override
  List get props => [todolist];
}

class Error extends TodoState {
  final String message;

  Error({@required this.message});

  @override
  // TODO: implement props
  List get props => [message];
}
