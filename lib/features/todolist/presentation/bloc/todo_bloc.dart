import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todo_detail/presentation/bloc/todo_detail_bloc_bloc.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/delete_todo_usecase.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/fetch_tasks.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/reorder_todolist_usecase.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_event.dart';
import 'package:flutter_todo/features/todolist/presentation/bloc/todo_state.dart';

class TodoBloc extends Bloc<TodoEvent, TodoState> {
  final FetchTaskListUsecase taskListUsecase;
  final DeleteTodoUseCase deleteTodoUseCase;
  final TodoReorderUseCase reorderUseCase;

  TodoBloc({
    @required this.taskListUsecase,
    @required this.deleteTodoUseCase,
    @required this.reorderUseCase,
  });

  @override
  TodoState get initialState => Empty();

  @override
  Stream<TodoState> mapEventToState(TodoEvent event) async* {
    if (event is GetTodoList) {
      yield Loading();

      final todoList = await taskListUsecase(NoParams());

      if (todoList == null) {
        yield Empty();
        return;
      }

      yield* todoList.fold(
        (failure) async* {
          yield Error(message: failure.toString());
        },
        (success) async* {
          yield Loaded(todolist: success);
        },
      );
    } else if (event is DeleteTodoEvent) {
      final list = (currentState as Loaded).todolist;

      yield Loading();

      final result = await deleteTodoUseCase(event.documentId);

      if (list == null || list.isEmpty) {
        yield Empty();
      }

      print(list.length);

      yield* result.fold((failure) async* {
        print(failure.toString());
        yield Error(message: failure.toString());
      }, (success) async* {
        list.removeWhere((el) => el.docId == event.documentId);
        yield Loaded(todolist: list);
      });
    } else if (event is TodoReOrderEvent) {
      final result = await reorderUseCase(event.todo);

      yield* result.fold((failure) async* {
        yield Error(message: ERROR_WHILE_PERFORMING_TASK);
      }, (success) async* {});
    }
  }
}
