import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

@immutable
abstract class TodoEvent extends Equatable {
  @override
  List get props => [];
}

class GetTodoList extends TodoEvent {}

class DeleteTodoEvent extends TodoEvent {
  final String documentId;

  DeleteTodoEvent({
    @required this.documentId,
  });
}

class TodoReOrderEvent extends TodoEvent {
  final Todo todo;

  TodoReOrderEvent({@required this.todo});
}
