import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';

class TaskRepositoryImpl implements TaskRepository {
  final FirebaseDataSource dataSource;

  TaskRepositoryImpl({this.dataSource});

  @override
  Future<Either<Failure, List<Todo>>> getTaskList() async {
    try {
      final todolist = await dataSource.getTodoList();
      return Right(todolist);
    } catch (error) {
      return Left(TaskFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> deleteTask(String docId) async {
    try {
      final result = await dataSource.deleteTask(docId);
      return Right(result);
    } catch (error) {
      return Left(error);
    }
  }

  @override
  Future<Either<Failure, String>> reorderTodoList(Todo todo) async {
    try {
      await dataSource.reorderList(todo);
      return Right(todo.docId);
    } catch (error) {
      print(error.toString());
      return Left(TaskFailure());
    }
  }
}
