import 'package:equatable/equatable.dart';

class Todo extends Equatable {
  final String docId;
  String name;
  String description;
  final bool isCompleted;
  double sortNumber;

  Todo({
    this.name,
    this.description,
    this.isCompleted,
    this.docId,
    this.sortNumber,
  });

  @override
  // TODO: implement props
  List get props => [name, description, isCompleted];

  void setName(String name) {
    this.name = name;
  }

  void setDescription(String description) {
    this.description = description;
  }

  void setSortNumber(double number) {
    this.sortNumber = number;
  }
}
