import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

abstract class TaskRepository {
  Future<Either<Failure, List<Todo>>> getTaskList();

  Future<Either<Failure, bool>> deleteTask(String docId);

  Future<Either<Failure, String>> reorderTodoList(Todo todo);
}
