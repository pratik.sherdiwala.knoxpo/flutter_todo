import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';

class FetchTaskListUsecase implements UseCase<List<Todo>, NoParams> {
  final TaskRepository taskRepository;

  FetchTaskListUsecase({this.taskRepository});
  @override
  Future<Either<Failure, List<Todo>>> call(NoParams params) async {
    return await taskRepository.getTaskList();
  }
}
