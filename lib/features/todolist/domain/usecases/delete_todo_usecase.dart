import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';

class DeleteTodoUseCase implements UseCase<bool, String> {
  final TaskRepository taskRepository;

  DeleteTodoUseCase({this.taskRepository});

  @override
  Future<Either<Failure, bool>> call(String params) async {
    final result = await taskRepository.deleteTask(params);
    return result;
  }
}
