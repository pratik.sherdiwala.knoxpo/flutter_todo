import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';
import 'package:flutter_todo/features/todolist/domain/repositories/task_repository.dart';

class TodoReorderUseCase implements UseCase<void, Todo> {
  final TaskRepository repository;

  TodoReorderUseCase({this.repository});

  @override
  Future<Either<Failure, void>> call(Todo params) {
    return repository.reorderTodoList(params);
  }
}
