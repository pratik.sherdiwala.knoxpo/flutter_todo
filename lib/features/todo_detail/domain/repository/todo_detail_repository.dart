import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

abstract class TodoDetailRepository {
  Future<Either<Failure, String>> updateTask(Todo todo);

  Future<Either<Failure, String>> insertTask(Todo todo);
}
