import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class UpdateTaskUseCase implements UseCase<void, Todo> {
  final TodoDetailRepository todoDetailRepository;

  UpdateTaskUseCase({this.todoDetailRepository});

  @override
  Future<Either<Failure, String>> call(Todo params) {
    return todoDetailRepository.updateTask(params);
  }
}
