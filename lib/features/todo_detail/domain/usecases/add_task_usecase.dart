import 'package:dartz/dartz.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/usecases/usecase.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class AddTaskUseCase implements UseCase<String, Todo> {
  final TodoDetailRepository todoDetailRepository;

  AddTaskUseCase({this.todoDetailRepository});

  @override
  Future<Either<Failure, String>> call(Todo params) async {
    return await todoDetailRepository.insertTask(params);
  }
}
