import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class TodoDetailBlocState extends Equatable {
  @override
  List get props => [];
}

class Empty extends TodoDetailBlocState {}

class Loading extends TodoDetailBlocState {}

class Loaded extends TodoDetailBlocState {
  final String documentId;

  Loaded({this.documentId});

  @override
  // TODO: implement props
  List get props => [documentId];
}

class Error extends TodoDetailBlocState {
  final String message;

  Error({this.message});

  @override
  // TODO: implement props
  List get props => [message];
}
