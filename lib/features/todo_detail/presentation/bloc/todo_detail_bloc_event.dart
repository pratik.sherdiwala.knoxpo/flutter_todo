import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

abstract class TodoDetailBlocEvent extends Equatable {
  @override
  // TODO: implement props
  List get props => [];
}

class AddTodoEvent extends TodoDetailBlocEvent {
  final Todo todo;

  AddTodoEvent({@required this.todo});
}

class UpdateTodoEvent extends TodoDetailBlocEvent {
  final Todo todo;

  UpdateTodoEvent({this.todo});
}
