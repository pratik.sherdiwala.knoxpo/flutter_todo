import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/add_task_usecase.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/update_task_usecase.dart';

import './bloc.dart';

const ERROR_WHILE_PERFORMING_TASK = 'Error while performing task.';

class TodoDetailBlocBloc
    extends Bloc<TodoDetailBlocEvent, TodoDetailBlocState> {
  final AddTaskUseCase addTaskUseCase;
  final UpdateTaskUseCase updateTaskUseCase;

  TodoDetailBlocBloc(
      {@required this.addTaskUseCase, @required this.updateTaskUseCase});

  @override
  TodoDetailBlocState get initialState => Empty();

  @override
  Stream<TodoDetailBlocState> mapEventToState(
    TodoDetailBlocEvent event,
  ) async* {
    if (event is AddTodoEvent) {
      yield Loading();

      final result = await addTaskUseCase(event.todo);

      yield* result.fold((failure) async* {
        yield Error(message: ERROR_WHILE_PERFORMING_TASK);
        print(failure.toString());
      }, (success) async* {
        yield Loaded(documentId: success);
        print('success');
      });
    } else if (event is UpdateTodoEvent) {
      yield Loading();

      final result = await updateTaskUseCase(event.todo);

      yield* result.fold(
        (failure) async* {
          yield Error(message: ERROR_WHILE_PERFORMING_TASK);
        },
        (success) async* {
          yield Loaded(documentId: success);
        },
      );
    }
  }
}
