import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/features/todo_detail/presentation/bloc/bloc.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

import '../../../../injection_container.dart';

class AddTodoPage extends StatefulWidget {
  final Todo todo;
  final double sortNumber;

  AddTodoPage({this.todo, this.sortNumber});

  @override
  _AddTodoPageState createState() => _AddTodoPageState();
}

class _AddTodoPageState extends State<AddTodoPage> {
  TodoDetailBlocBloc todoDetailBloc;

  TextEditingController nameController = TextEditingController();

  TextEditingController descController = TextEditingController();

  @override
  void initState() {
    super.initState();
    todoDetailBloc = sl<TodoDetailBlocBloc>();
    if (widget.todo != null) {
      nameController.value = TextEditingValue(text: widget.todo.name);
      descController.value = TextEditingValue(text: widget.todo.description);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Add Task'),
        ),
        body: BlocListener<TodoDetailBlocBloc, TodoDetailBlocState>(
          bloc: todoDetailBloc,
          listener: (_, state) {
            if (state is Loaded) {
              final todoMap = {
                'todo': TodoModel(
                  name: nameController.text,
                  description: descController.text,
                  isCompleted: false,
                  docId: state.documentId,
                  sortNumber: widget.sortNumber + 1.0,
                )
              };
              Navigator.pop(context, todoMap);
            }
          },
          child: BlocBuilder<TodoDetailBlocBloc, TodoDetailBlocState>(
            bloc: todoDetailBloc,
            builder: (_, state) {
              if (state is Empty) {
              } else if (state is Loading) {
                return Center(child: CircularProgressIndicator());
              }
              return Center(
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: nameController,
                      decoration: InputDecoration(hintText: 'Enter todo title'),
                    ),
                    TextField(
                      controller: descController,
                      decoration:
                          InputDecoration(hintText: 'Enter description'),
                    ),
                    FlatButton(
                      onPressed: () {
                        if (widget.todo != null) {
                          widget.todo.setName(nameController.text);
                          widget.todo.setDescription(descController.text);
                          todoDetailBloc
                              .onEvent(UpdateTodoEvent(todo: widget.todo));
                        } else {
                          todoDetailBloc.onEvent(AddTodoEvent(
                              todo: Todo(
                            name: nameController.text,
                            description: descController.text,
                            sortNumber: widget.sortNumber,
                          )));
                        }
                      },
                      child: Text(
                        widget.todo != null ? 'Update Todo' : 'Add Todo',
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue.shade400,
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

//class InputTextDetail extends StatelessWidget {
//  final String hintText;
//
//  InputTextDetail({this.hintText});`
//
//  @override
//  Widget build(BuildContext context) {
//    return TextField(
//      decoration: InputDecoration(hintText: hintText),
//    );
//  }
//}
