import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/core/data/datasource/firebase_data_source.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class TodoDetailRepositoryImpl implements TodoDetailRepository {
  final FirebaseDataSource dataSource;

  TodoDetailRepositoryImpl({@required this.dataSource});

  @override
  Future<Either<Failure, String>> insertTask(Todo todo) async {
    try {
      final documentId = await dataSource.insertTask(todo);
      return Right(documentId);
    } catch (error) {
      return Left(error);
    }
  }

  @override
  Future<Either<Failure, String>> updateTask(Todo todo) async {
    try {
      await dataSource.updateTask(todo);
      return Right(todo.docId);
    } catch (error) {
      return Left(error);
    }
  }
}
