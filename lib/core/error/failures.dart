import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List get props => [];
}

class ConnectionFailure extends Failure {}

class TaskFailure extends Failure {}
