import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/core/error/failures.dart';
import 'package:flutter_todo/core/util/extentions/converters.dart';
import 'package:flutter_todo/core/util/to_map_converter.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

abstract class FirebaseDataSource {
  Future<List<TodoModel>> getTodoList();

  Future<String> insertTask(Todo todoModel);

  Future<bool> deleteTask(String docId);

  Future<String> updateTask(Todo todo);

  Future<String> reorderList(Todo todo);
}

class FirebasedataSourceImpl implements FirebaseDataSource {
  final Firestore firestore;
  final ToMapConverter converter;

  FirebasedataSourceImpl({@required this.firestore, @required this.converter});

  @override
  Future<List<TodoModel>> getTodoList() {
    List<TodoModel> todoList = [];
    return firestore.collection('todo').getDocuments().then((value) {
      value.documents.forEach((docTask) {
        todoList.add(docTask.fromMap());
        //todoList.add(TodoModel.fromMap(docTask));
      });
      return todoList;
    }).catchError((error) {
      throw TaskFailure();
    });
  }

  @override
  Future<String> insertTask(Todo todoModel) async {
    final todoMap = converter.toMap(todoModel);

    print(todoModel.name);
    print(todoModel.description);
    return await firestore.collection('todo').add(todoModel.toMap())
        //.add(todoMap)
        .then((val) {
      return val.documentID;
    }).catchError((error) {
      print(error.toString());
      throw error;
    });
  }

  @override
  Future<bool> deleteTask(String docId) async {
    print(docId);
    return await firestore
        .collection('todo')
        .document(docId)
        .delete()
        .then((val) {
      return true;
    }).catchError((error) {
      print(error.toString());
      throw error;
    });
  }

  @override
  Future<String> updateTask(Todo todo) async {
    return await firestore.collection('todo').document(todo.docId).updateData(
      {
        'name': todo.name,
        'description': todo.description,
      },
    ).then((val) {
      return todo.docId;
    }).catchError((error) {
      print(error.toString());
      throw error;
    });
  }

  @override
  Future<String> reorderList(Todo todo) {
    return firestore.collection('todo').document(todo.docId).setData({
      'name': todo.name,
      'description': todo.description,
      'sortNumber': todo.sortNumber,
      'isCompleted': todo.isCompleted,
    }).then((value) {
      return todo.docId;
    }).catchError((error) {
      throw error;
    });
  }
}
