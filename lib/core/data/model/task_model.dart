import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class TodoModel extends Todo {
  TodoModel({
    @required String docId,
    @required String name,
    @required String description,
    @required bool isCompleted,
    double sortNumber,
  }) : super(
            docId: docId,
            name: name,
            description: description,
            isCompleted: isCompleted,
            sortNumber: sortNumber);

  factory TodoModel.fromMap(DocumentSnapshot documentSnapshot) {
    return TodoModel(
      docId: documentSnapshot['docId'],
      name: documentSnapshot['name'],
      description: documentSnapshot['description'],
      isCompleted: documentSnapshot['isCompleted'],
      sortNumber: documentSnapshot['sortNumber'],
    );
  }
}
