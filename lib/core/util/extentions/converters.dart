import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_todo/core/data/model/task_model.dart';
import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

extension Mapping on Todo {
  Map<String, dynamic> toMap() => {
        'name': this.name,
        'description': this.description,
        'isCompleted': false,
        'sortNumber': this.sortNumber,
      };
}

extension DocumentFromMap on DocumentSnapshot {
  TodoModel fromMap() => TodoModel(
      docId: this.documentID,
      name: this.data['name'],
      description: this.data['description'],
      isCompleted: this.data['isCompleted'],
      sortNumber: this.data['sortNumber'] as num);
}
