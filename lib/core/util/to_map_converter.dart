import 'package:flutter_todo/features/todolist/domain/entities/task.dart';

class ToMapConverter {
  Map<String, dynamic> toMap(Todo todo) {
    return {
      'name': todo.name,
      'description': todo.description,
      'isCompleted': todo.isCompleted,
    };
  }
}
