import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_todo/features/todo_detail/data/repositories/todo_detail_repository_impl.dart';
import 'package:flutter_todo/features/todo_detail/domain/repository/todo_detail_repository.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/add_task_usecase.dart';
import 'package:flutter_todo/features/todo_detail/domain/usecases/update_task_usecase.dart';
import 'package:flutter_todo/features/todo_detail/presentation/bloc/bloc.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/delete_todo_usecase.dart';
import 'package:flutter_todo/features/todolist/domain/usecases/reorder_todolist_usecase.dart';
import 'package:get_it/get_it.dart';

import 'core/data/datasource/firebase_data_source.dart';
import 'core/util/to_map_converter.dart';
import 'features/todolist/data/repositories/task_repository_impl.dart';
import 'features/todolist/domain/repositories/task_repository.dart';
import 'features/todolist/domain/usecases/fetch_tasks.dart';
import 'features/todolist/presentation/bloc/todo_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerFactory(
    () => TodoBloc(
        taskListUsecase: sl(), deleteTodoUseCase: sl(), reorderUseCase: sl()),
  );

  sl.registerFactory(
      () => TodoDetailBlocBloc(addTaskUseCase: sl(), updateTaskUseCase: sl()));

  sl.registerLazySingleton(() => AddTaskUseCase(todoDetailRepository: sl()));

  sl.registerLazySingleton(() => FetchTaskListUsecase(taskRepository: sl()));

  sl.registerLazySingleton(() => DeleteTodoUseCase(taskRepository: sl()));

  sl.registerLazySingleton(() => UpdateTaskUseCase(todoDetailRepository: sl()));

  sl.registerLazySingleton(() => TodoReorderUseCase(repository: sl()));

  sl.registerLazySingleton<TaskRepository>(
      () => TaskRepositoryImpl(dataSource: sl()));

  sl.registerLazySingleton<TodoDetailRepository>(
      () => TodoDetailRepositoryImpl(dataSource: sl()));

  sl.registerLazySingleton<FirebaseDataSource>(
      () => FirebasedataSourceImpl(firestore: sl(), converter: sl()));

  sl.registerLazySingleton<ToMapConverter>(() => ToMapConverter());

  sl.registerLazySingleton<Firestore>(() => Firestore.instance);
}
